package main

import (
	"fmt"

	"gitee.com/sillyman/mixlog"
	"github.com/labstack/echo/v4"

	"gitee.com/sillyman/PingHostsWebView/api"
	. "gitee.com/sillyman/PingHostsWebView/modules/config"
)

const (
	APPTitle         = "PING监视器"
	CompanyName      = "xx有限公司"
	CompanyShortName = "xx"
	CompanyWebsite   = "http://www.xxx.com.cn/"
	CompanyDescr     = "xx是国内一流的xx产品厂家。"
)

func main() {
	e := echo.New()
	mustSetEcho(e, Cfg.Debug)
	mixlog.Info("已设置 WEB 引擎")

	// 注册 APIs 路由
	e.GET("/hosts", api.GetHosts)
	e.GET("/hosts/:ip", api.GetHost)
	e.GET("/hosts/running/ip_addresses", api.GetRunningHostsIPs)
	e.POST("/hosts", api.AddHost)
	e.PUT("/hosts/:ip", api.UpdateHost)
	e.DELETE("/hosts/:ip", api.DelHost)
	e.PATCH("/hosts/is_running/:ip", api.SetHostRunningStatus)

	e.GET("/pingers/summaries", api.GetPingSummaries)
	e.DELETE("/pingers/summaries", api.ClearAllSummaries)
	e.PATCH("/pingers/summaries/comment/:ip", api.SetPingSummaryComment)
	e.GET("/ws/pingers/summaries", api.WsBroLatestPingSummaries)

	e.GET("/ping/records/count/:ip", api.CountPingRecords)
	e.GET("/ping/records/:ip", api.GetPingRecords)

	mixlog.Debug("已经注册APIs路由")

	// 前端页面
	e.GET("/", func(c echo.Context) error {
		return c.Render(200, "index.html", echo.Map{
			"title":            APPTitle,
			"companyName":      CompanyName,
			"shortCompanyName": CompanyShortName,
			"companyWebsite":   CompanyWebsite,
		})
	})
	mixlog.Debug("已经注册前端页面")

	mixlog.Infof("WEB 服务开始监听：%s", Cfg.HTTPListeningOnAddr)

	fmt.Println("==============================")
	fmt.Printf("本软件由%s开发，供客户免费使用。\n", CompanyShortName)
	fmt.Println(CompanyDescr)
	fmt.Printf("公司官网：%s\n", CompanyWebsite)
	fmt.Println("==============================")

	e.Logger.Fatal(e.Start(Cfg.HTTPListeningOnAddr))
}
