package main

import (
	"os"

	"gitee.com/sillyman/mixlog"

	. "gitee.com/sillyman/PingHostsWebView/modules/config"
	"gitee.com/sillyman/PingHostsWebView/modules/db"
	"gitee.com/sillyman/PingHostsWebView/modules/mypinger"
)

func init() {
	// 创建必须的目录
	if err := os.MkdirAll("./data", 0755); err != nil {
		panic(err)
	}

	// 加载配置
	MustLoadConfig()

	// 初始化日志记录器
	MustInitMixlog(Cfg.Debug)
	mixlog.Info("已初始化 logger")

	// 打开数据库连接
	db.MustObtainMainDB()
	db.ReleaseMainDB()
	mixlog.Info("已建立数据库会话")

	// 启动pinger Mgr
	go mypinger.MustStartPingerMgr()
	mixlog.Info("已启动Pinger管理器")
}
