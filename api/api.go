package api

import (
	"fmt"

	"github.com/labstack/echo/v4"
)

var (
	newHttpErrReadDB = func(err error) error {
		return echo.NewHTTPError(500, echo.Map{"code": 500, "msg": fmt.Sprintf("读数据库发生错误：%s", err.Error())})
	}
	newHttpErrWriteDB = func(err error) error {
		return echo.NewHTTPError(500, echo.Map{"code": 500, "msg": fmt.Sprintf("写数据库发生错误：%s", err.Error())})
	}
	newHttpErrServer = func(err error) error {
		return echo.NewHTTPError(500, echo.Map{"code": 500, "msg": fmt.Sprintf("服务器错误：%s", err.Error())})
	}
	newHttpErrBadRequest = func(err error) error {
		return echo.NewHTTPError(400, echo.Map{"code": 400, "msg": fmt.Sprintf("错误的请求：%s", err.Error())})
	}
	newHttpErrBadParam = func(err error) error {
		return echo.NewHTTPError(400, echo.Map{"code": 400, "msg": fmt.Sprintf("错误的参数：%s", err.Error())})
	}
	newHttpErr404 = func() error {
		return echo.NewHTTPError(404, echo.Map{"code": 404, "msg": "资源不存在"})
	}
)

var (
	respJSON200 = func(c echo.Context, data interface{}) error { return c.JSON(200, echo.Map{"code": 200, "data": data}) }
	respJSON201 = func(c echo.Context, data interface{}) error { return c.JSON(201, echo.Map{"code": 201, "data": data}) }
	resp202     = func(c echo.Context) error { return c.NoContent(202) }
	resp204     = func(c echo.Context) error { return c.NoContent(204) }
)
