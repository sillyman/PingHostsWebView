package models

import (
	"time"
)

type HostConfig struct {
	ID        uint      `gorm:"primarykey" json:"id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`

	// IP 目标主机的IP地址
	IP string `gorm:"uniqueIndex" json:"ip" validate:"required,ip"`

	// PingTimeout ping超时
	PingTimeout time.Duration `gorm:"default:3000000000" json:"ping_timeout"`

	// PingPayloadSize 荷载
	PingPayloadSize uint16 `gorm:"default:22" json:"ping_payload_size"`

	// pingIntervalDur 下一次ping的间隔时长
	PingIntervalDur time.Duration `gorm:"default:3000000000" json:"ping_interval_dur"`

	// NumberOfMaxRecord 最大记录数量
	NumberOfMaxRecord uint `gorm:"default:604800" json:"number_of_max_record"`

	// IsRunning
	IsRunning bool `json:"is_running"`
}
