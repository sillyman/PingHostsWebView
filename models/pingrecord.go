package models

import (
	"time"
)

// PingRecord
type PingRecord struct {
	ID        uint          `gorm:"primarykey" json:"-"`
	CreatedAt time.Time     `gorm:"index" json:"created_at"`
	RTT       time.Duration `gorm:"column:rtt" json:"rtt"`
}
