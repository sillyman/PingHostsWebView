package models

import (
	"time"
)

// PingSummary 目标主机的信息总结
type PingSummary struct {
	ID        uint      `gorm:"primarykey" json:"id"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`

	// IP 目标主机的IP地址
	IP string `gorm:"uniqueIndex" json:"ip"`

	// SucceedCount 成功次数
	SucceedCount uint `json:"succeed_count"`

	// FailedCount 失败次数
	FailedCount uint `json:"failed_count"`

	// MaxRTT 最大往返时长
	MaxRTT time.Duration `gorm:"column:max_rtt" json:"max_rtt"`

	// MinRTT 最小往返时长
	MinRTT time.Duration `gorm:"column:min_rtt" json:"min_rtt"`

	// AvgRTT 平均往返时长
	AvgRTT time.Duration `gorm:"column:avg_rtt" json:"avg_rtt"`

	// SumRTTSquares 用于计算 mdev， sumSquares += (rtt - avg) * (rtt - avg)
	SumRTTSquares time.Duration `gorm:"column:sum_rtt_squares" json:"-"`

	// mdev = Mean Deviation 这个值越大说明你的网速越不稳定
	MDevRTT time.Duration `gorm:"column:mdev_rtt" json:"mdev_rtt"`

	// TotalRTT rtt总和
	TotalRTT time.Duration `gorm:"column:total_rtt" json:"-"`

	// LastStatus 上一次往返时长，如果是失败则为负数
	LastRTT time.Duration `gorm:"column:last_rtt" json:"last_rtt"`

	// LastFailedAt 上一次失败的时间
	LastFailedAt time.Time `json:"last_failed_at"`

	// Comment 备注
	Comment string `gorm:"type:VARCHAR(64)" json:"comment"`
}
