package config

import (
	"io/ioutil"
	"os"

	"github.com/pelletier/go-toml"
)

var (
	// cfgFilePth 配置文件路径
	cfgFilePth = "./config.toml"

	// AllowMaxNumHosts 系统允许管理的最大主机数量
	AllowMaxNumHosts = 100

	// Cfg 程序运行时的配置
	Cfg = &Config{
		HTTPListeningOnAddr: "0.0.0.0:8016",
		Debug:               false,
	}
)

// Config
type Config struct {
	// HTTPListeningOnAddr HTTP监听地址
	HTTPListeningOnAddr string `toml:"http_listening_on_addr" comment:"HTTP服务监听的地址"`

	// Debug 详细日志会输出到文件
	Debug bool `toml:"debug" comment:"设置为 true 则会输出详细日志到 ./debug.log 文件"`
}

// MustLoadConfig 加载配置文件
func MustLoadConfig() {
	buf, err := ioutil.ReadFile(cfgFilePth)
	if err != nil {
		if os.IsNotExist(err) {
			mustSaveConfig()
			return
		}
		panic(err)
	}
	if err := toml.Unmarshal(buf, Cfg); err != nil {
		panic(err)
	}
}

// mustSaveConfig 保存配置到文件
func mustSaveConfig() {
	buf, _ := toml.Marshal(Cfg)
	err := ioutil.WriteFile(cfgFilePth, buf, 0644)
	if err != nil {
		panic(err)
	}
}
