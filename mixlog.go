package main

import (
	"os"
	"path/filepath"

	"gitee.com/sillyman/mixlog"
)

// debugFilePth 当debug开启时，详细的日志输出到此文件
const debugFilePth = "./logs/debug.log"

// MustInitMixlog 初始化日志系统
func MustInitMixlog(debug bool) {
	stdout := mixlog.NewHandlerToWriter(
		mixlog.LvlInfo,
		mixlog.MustNewFormatter("{{.Time}} - {{.Lvl}} - {{.Message}}", "15:04:05.99"),
		os.Stdout,
		false,
	)

	if debug {
		if err := os.MkdirAll(filepath.Dir(debugFilePth), 0755); err != nil {
			panic(err)
		}

		mixlog.SetGlobalHandlers(
			stdout,
			mixlog.MustNewHandlerToFile(
				mixlog.LvlDebug,
				mixlog.MustNewFormatter(mixlog.FormatFuncNameShortFileName, "2006-01-02T15:04:05.000-07:00"),
				debugFilePth,
				false,
			),
		)

	} else {
		mixlog.SetGlobalHandlers(stdout)
	}
}
